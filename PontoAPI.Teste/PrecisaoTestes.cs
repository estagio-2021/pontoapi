using System;
using Xunit;

namespace PontoAPI.Teste
{
    public class PrecisaoTest
    {
        [Fact]
        public void precisaoTestValido()
        {
            Precisao precisao = new Precisao(2);
            Assert.Equal(2, precisao.ParaInteiro());
        }

        [Fact]
        public void precisaoTestInvalido()
        {
            Precisao precisao = new Precisao(2);
            Assert.NotEqual(2.5, precisao.ParaInteiro());
        }
    }
}