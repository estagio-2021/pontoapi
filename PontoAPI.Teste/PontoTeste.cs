using System;
using Xunit;

namespace PontoAPI.Teste
{
    public class PontoTeste
    {
        [Fact]
        public void testeDeDistanciaValido()
        {
           Ponto pontoA = new Ponto(0,0);
           Ponto pontoB = new Ponto(2,3);
           
            
           Assert.Equal(3.61, pontoA.Distancia(pontoB).ParaDouble(new Precisao(2)));
        }
        [Fact]
        public void testeDeDistanciaInvalido()
        {
           Ponto pontoA = new Ponto(0,0);
           Ponto pontoB = new Ponto(2,3);
           
            
           Assert.NotEqual(3.60, pontoA.Distancia(pontoB).ParaDouble(new Precisao(2)));
        }
        
        
    }
}