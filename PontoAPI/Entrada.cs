namespace PontoAPI
{
    public class Entrada
    {
        private Ponto pontoA;
        private Ponto pontoB;
        private Precisao precisao;
        public Entrada(Ponto pontoA, Ponto pontoB, Precisao precisao)
        {
            this.pontoA = pontoA;
            this.pontoB = pontoB;
            this.precisao = precisao;
        }

        public double ParaDouble()
        {
            return pontoA.Distancia(pontoB).ParaDouble(precisao);
        }
        
    }
}