﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace PontoAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PontoController : ControllerBase
    {
        private readonly ILogger<PontoController> _logger;

        public PontoController(ILogger<PontoController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public double Post([FromBody] Entrada)
        {
            return a.Distancia(b).ParaDouble(new Precisao(precisao));
        }
    }
}