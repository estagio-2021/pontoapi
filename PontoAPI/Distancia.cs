using System;

namespace PontoAPI
{
    public class Distancia
    {
        private readonly float distancia;

        public Distancia(float distancia)
        {
            this.distancia = distancia;
        }

        public double ParaDouble(Precisao precisao)
        {
            return Math.Round(distancia, precisao.ParaInteiro());
        }
    }
}