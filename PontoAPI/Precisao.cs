namespace PontoAPI
{
    public class Precisao
    {
        private int precisao;

        public Precisao(int precisao)
        {
            this.precisao = precisao;
        }

        public int ParaInteiro()
        {
            if(precisao >= 0) throw new ArgumentException("Precisão inválida");
            
            
        }
    }
}