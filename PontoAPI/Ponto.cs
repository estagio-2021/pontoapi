using System;


namespace PontoAPI
{
    public class Ponto
    {
       private float X;
       private float Y;


       public Ponto(float X, float Y)
       {
           this.X = X;
           this.Y = Y;
           
       }

       public Distancia Distancia(Ponto B)
       {
            float distancia = Raiz(Quadrado(B.X - X) + Quadrado(B.Y -Y));
            return new Distancia(distancia);
       }

       private float Raiz(float numero)
       {
           return ((float)Math.Sqrt(numero));
       }

       private float Quadrado(float numero)
       {
           return numero * numero;
       }
    }
}